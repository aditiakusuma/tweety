<?php

namespace App\Http\Controllers;

use App\Followable;
use Illuminate\Http\Request;
use App\User;

class FollowsController extends Controller
{

    public function store(User $user)
    {

        if (auth()->user()->toggleFollow($user) === 1) {
            return back()->with('unfollow', $user->username);
        }

        return back()->with('follow', $user->username);
    }
}
