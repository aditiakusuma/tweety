<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Validation\Rule;

class ProfilesController extends Controller
{
    public function show(User $user) //$id di ambil dari wildcard yang di tulis dan di bind ke User
    {
        //return $user; //memberikan nilai user yang id nya sesuai wildcard
        return view('profiles.show', [
            'user' => $user,
            'tweets' => $user->tweets()->withLikes()->get(),
        ]);
    }

    public function edit(User $user)
    {
        $this->authorize('edit', $user);
        return view('profiles.edit', compact('user'));
    }

    public function update(User $user)
    {


        $attributes = request()->validate([
            'username' => ['string', 'required', 'max:255', Rule::unique('users')->ignore($user)],
            'name' => ['string', 'required', 'max:255'],
            'avatar' => ['file'],
            'email' => ['string', 'required', 'max:255', 'email', Rule::unique('users')->ignore($user)],
            'password' => ['string', 'required', 'min:8', 'max:255', 'confirmed']
        ]);

        if (request('avatar')) {
            $attributes['avatar'] = request('avatar')->store('avatars');
        }


        $user->update($attributes);

        return redirect($user->path());
    }

    public function editBanner(User $user)
    {
        return view('profiles.edit-banner', compact('user'));
    }

    public function updateBanner(User $user)
    {
        $attributes = request()->validate([
            'banner' => ['file']
        ]);

        if (request('banner')) {
            $attributes['banner'] = request('banner')->store('banners');
        }


        $user->update($attributes);

        return redirect($user->path());
    }

    public function editDescription(User $user)
    {
        return view('profiles.edit-description', compact('user'));
    }

    public function updateDescription(User $user)
    {
        $attributes = request()->validate([
            'description' => ['string', 'max:255']
        ]);

        if (request('description')) {
            $attributes['description'] = request('description');
        }


        $user->update($attributes);

        return redirect($user->path());
    }
}
