<?php

namespace App\Http\Controllers;

use App\Tweet;
use Illuminate\Http\Request;

class TweetController extends Controller
{
    public function index()
    {
        return view('tweets.index', [
            //ambil hanya timeline dari user, timeline di ambil dari user model method timeline
            'tweets' => auth()->user()->timeline()
        ]);
    }

    public function store()
    {

        // validasi menghasilkan array atrribute
        $attribute = request()->validate([
            'body' => ['required', 'max:255'],
            'image' => ['file'],
        ]);



        if (request('image')) {
            $attribute['image'] = request('image')->store('posts');
        } else {
            $attribute['image'] = "";
        }



        Tweet::create([
            'user_id' => auth()->id(),
            'body' => $attribute['body'], // masuk dari array
            'image' => $attribute['image']
        ]);


        return redirect('/tweets')->with('message', ' Tweet Updated');
    }

    public function destroy($id)
    {
        Tweet::where('id', $id)->delete();
        return back()->with('message', 'Tweet Deleted');
    }
}
