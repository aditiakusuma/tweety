<x-app>
    <div>
        @foreach ($users as $user)
        <a href="{{$user->path()}}" class="flex items-center mb-5 mr-2">

            <img src="{{$user->avatar}}" alt="avatar" style="
            width: 50px;
            height: 50px;" class="rounded-full">


            <h4 class="font-bold px-4"> {{'@'.$user->username}}</h4>
        </a>

        @endforeach
        <div class="pagination">
            {{ $users->links() }}
        </div>
    </div>
</x-app>