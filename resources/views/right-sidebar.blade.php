<h3 class="font-bold text-xl mb-4">Friends</h3>
<ul>
    @forelse (auth()->user()->follows as $friend)
    <li>
        <a href="{{route('profile', $friend->username)}}">
            <div class="flex items-center text-sm mb-2">
                <img src="{{$friend->avatar}}" alt="" class="rounded-full mr-2" style="
                width: 40px;
                height: 40px;">
                {{$friend->name}}
            </div>
        </a>
    </li>
    @empty

    <p class=" p-4">No Friends</p>
    @endforelse

</ul>