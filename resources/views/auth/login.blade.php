<x-master>
    <div class="flex justify-center items-start h-screen" style="background-color: #33CCFF">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="w-full">
                    <div class="text-center text-white font-bold text-lg p-4">{{ __('Login') }}</div>

                    <div class="bg-white shadow-md rounded px-8 py-6 mb-4">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="row">
                                <label for="email"
                                    class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                        class="shadow appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-1 px-2 mb-2 leading-tight focus:outline-none focus:bg-white @error('email') is-invalid @enderror"
                                        name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="text-red-700 text-xs" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                        class="shadow appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-1 px-2 mb-2 leading-tight focus:outline-none focus:bg-white @error('password') is-invalid @enderror"
                                        name="password" required autocomplete="current-password">

                                    @error('password')
                                    <span class="text-red-700 text-xs" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                            {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="rounded mx-auto py-2 px-4 text-white uppercase"
                                        style="background-color: #33CCFF">
                                        {{ __('Login') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                    @endif

                                    <div class="py-2"> <a href="/register" class="text-blue-500 underline">Register as
                                            New User</a></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-master>