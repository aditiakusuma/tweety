<x-master>
    <div class="flex justify-center items-start h-screen" style="background-color: #33CCFF">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="w-full">
                    <div class="text-center text-white font-bold text-lg p-4">{{ __('Register') }}</div>

                    <div class="bg-white shadow-md rounded px-8 py-6">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <div class=" row">
                                <label for="username" class="col-md-4 col-form-label text-md-right">Username</label>

                                <div class="col-md-6">
                                    <input id="username" type="text"
                                        class="shadow appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-1 px-2 mb-2 leading-tight focus:outline-none focus:bg-white @error('username') is-invalid @enderror"
                                        name="username" value="{{ old('username') }}" required autocomplete="username"
                                        autofocus>

                                    @error('username')
                                    <span class="text-red-700 text-xs" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                        class=" shadow appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-1 px-2 mb-2 leading-tight focus:outline-none focus:bg-white @error('name') is-invalid @enderror"
                                        name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="text-red-700 text-xs" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email"
                                    class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                        class=" shadow appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-1 px-2 mb-2 leading-tight focus:outline-none focus:bg-white @error('email') is-invalid @enderror"
                                        name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                    <span class="text-red-700 text-xs" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                        class="shadow appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-1 px-2 mb-2 leading-tight focus:outline-none focus:bg-white @error('password') is-invalid @enderror"
                                        name="password" required autocomplete="new-password">

                                    @error('password')
                                    <span class="text-red-700 text-xs" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password"
                                        class="shadow appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-1 px-2 mb-2 leading-tight focus:outline-none focus:bg-white"
                                        name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <div class="form-group row mb-0 my-4">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="rounded mx-auto py-2 px-4 text-white uppercase"
                                        style="background-color: #33CCFF">
                                        {{ __('Register') }}
                                    </button>
                                    <div class=" py-2">or
                                        <a href="/login" class="text-blue-500 underline">Login</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-master>