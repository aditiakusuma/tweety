<div class="flex items-center text-xs">
    <div class="{{ $tweet->isLikedBy(current_user()) ? 'text-blue-500' : 'text-gray-500'}} flex items-center">
        <form action="/tweets/{{$tweet->id}}/like" method="post">
            @csrf
            <button type="submit" class="mx-2">
                <i class="fa fa-thumbs-up  "></i>
            </button>
        </form>
        <div>
            {{$tweet->liked ?: 0}}
        </div>
    </div>
    <div class="{{ $tweet->isDislikedBy(current_user()) ? 'text-blue-500' : 'text-gray-500'}} flex items-center">
        <form action="/tweets/{{$tweet->id}}/like" method="post">
            @csrf
            @method('delete')
            <button type="submit" class="mx-2">
                <i class="fa fa-thumbs-down "></i>
            </button>
        </form>
        <div>
            {{$tweet->disliked ?: 0}}
        </div>
    </div>
</div>