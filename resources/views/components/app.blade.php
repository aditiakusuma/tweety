<x-master>
    <section class="px-8">
        <main class="container mx-auto mt-20">
            <div class="md:flex md:justify-between">
                {{-- Left SideBar --}}
                @if (auth()->check())
                <div class="md:w-32">
                    @include('left-sidebar')
                </div>
                @endif
                {{-- main --}}
                <div class="lg:mx-10 md:flex-1 md:mx-8" style="max-width: 700px">
                    {{ $slot }}
                </div>
                {{-- Right SideBar --}}
                @if (auth()->check())
                <div class="md:w-1/6 bg-blue-100 p-4 rounded-lg mt-4 md:mt-0">
                    @include('right-sidebar')
                </div>
                @endif
            </div>

        </main>
    </section>
</x-master>