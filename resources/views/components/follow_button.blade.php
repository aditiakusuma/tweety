@if (current_user()->isNot($user))
<form method="POST" action="/profiles/{{ $user->username}}/follow">
    @csrf
    <button type="submit" class="bg-blue-500 rounded-full shadow text-white py-2 px-4">
        {{current_user()->following($user) ? "UNFOLLOW ME" : "FOLLOW ME"}}
    </button>
</form>
@endif