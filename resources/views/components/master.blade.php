<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        #edit-banner:hover,
        #banner:hover+#edit-banner {
            opacity: 100;
            color: gray;
        }

        #desc:hover+#edit-desc,
        #tweet:hover #delete {
            opacity: 100;
            color: gray;
        }

        #edit-desc:hover {
            opacity: 100;
            color: gray;
        }

        .image-upload>input {
            display: none;
        }

        .image-upload i {
            width: 80px;
            cursor: pointer;
        }

        body {
            padding: 0;
            margin: 0;
        }
    </style>
</head>

{{-- body --}}

<body class=" w-screen h-full>
    <div id=" app" class=" w-screen h-full ">
    <section class="z-10 top-0 fixed px-8 py-4 w-full " style="background-color: #33CCFF">
        <header class="flex items-center justify-between ">

            <img style="width: 100px" src="/images/twitterlogo.png" alt="LOGO">
            <i class="material-icons text-white md:hidden">menu</i>
        </header>
    </section>

    {{ $slot }}

    </div>

    {{-- <script src="http://unpkg.com/turbolinks"></script> --}}
</body>

</html>