@can ('edit', $tweet->user)
<div class="absolute top-0 right-0 mr-4 mt-4 opacity-0" id="delete">
    <form action="tweets/{{$tweet->id}}/delete" method="post">
        @csrf
        @method('DELETE')
        <button type="submit">
            <i class="material-icons">delete_sweep</i>
        </button>
    </form>
</div>
@endcan