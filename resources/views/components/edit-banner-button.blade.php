@can('edit', $user)
<a href="{{ $user->path('edit-banner') }}" class="material-icons absolute top-0 right-0 opacity-0"
    id="edit-banner">mode_edit</a>
@endcan