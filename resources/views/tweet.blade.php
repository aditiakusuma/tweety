<div class="flex p-4 {{$loop->last ? '': 'border-b border-b-gray-400'}} relative" id="tweet">
    <div class="flex-shrink-0 mr-2">
        {{-- memberi link pd profile dan mengirim parameter wildcard berupa nama dari user tweet --}}
        <a href="{{route('profile', $tweet->user->username)}}">
            <img src="{{$tweet->user->avatar}}" alt="" class="rounded-full mr-2" style="
            width: 50px;
            height: 50px;">
        </a>
    </div>
    <div class="relative">
        <h5 class="font-bold mb-4">
            <a href="{{route('profile', $tweet->user->username)}}">
                {{$tweet->user->name}}
            </a>
        </h5>

        <p class="text-sm mb-4">
            {{$tweet->body}}
        </p>
        <div class="pb-4">
            <img src="{{$tweet->image}}" alt="">
        </div>

        <x-like-button :tweet="$tweet" />

    </div>
    <x-delete-post-button :tweet="$tweet" />

</div>