<x-app>

    @section('content')
    {{-- Profile --}}
    <div class=" mb-4">
        <div class="relative">

            <div class="" style="height: 300px;">
                <img class=" rounded-lg relative object-cover h-full w-full" src="{{$user->banner}}" alt="banner"
                    id="banner">
                <x-edit-banner-button :user="$user" />
            </div>
            <img src="{{$user->avatar}}"
                class="rounded-full mr-2 absolute bottom-0 transform -translate-x-1/2 translate-y-1/2" style="left: 50%;
                width: 150px;
                height: 150px;">

        </div>


        <div class=" flex justify-between items-center mb-6">
            <div>
                <h2 class="font-bold text-2xl">{{ $user->name}}</h2>
                <p class="text-sm">Motto</p>
            </div>
            <div class="flex ">
                @can ('edit', $user)
                <form action="{{ $user->path('edit') }}">
                    <button class=" rounded-full border border-gray-200 shadow text-black py-2 px-4">EDIT
                        PROFILE</button>
                </form>
                @endcan

                <x-follow_button :user="$user">
                </x-follow_button>
            </div>
        </div>
        <div class="flex justify-between relative">
            <p class="text-sm ml-3 text-center flex-1" id="desc">
                {{$user->description}}
            </p>
            @can('edit', $user)
            <div class="flex items-start">
                <a href="{{ $user->path('edit-description') }}" class="text-xs opacity-0 absolute right-0 top-0"
                    id="edit-desc"> Edit Description<i class="material-icons text-sm p-1">mode_edit</i> </a>

            </div>

            @endcan

        </div>

    </div>
    @if (session()->has('follow'))

    <div class="flex items-center bg-blue-400 text-white text-sm font-bold px-4 py-3" role="alert">
        <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
            <path
                d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z" />
        </svg>
        <p>{{session()->get('follow')}} has been followed</p>
    </div>

    @elseif (session()->has('unfollow'))

    <div class="flex items-center bg-red-400 text-white text-sm font-bold px-4 py-3" role="alert">
        <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
            <path
                d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z" />
        </svg>
        <p>{{session()->get('unfollow')}} has been unfollowed</p>
    </div>

    @endif

    {{-- Timeline --}}
    @include('timeline',[
    //kirimkan tweets dari user , diambil dari relasi
    'tweets' => $tweets,
    'user' => $user,
    ])
</x-app>