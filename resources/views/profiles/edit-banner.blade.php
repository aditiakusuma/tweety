<x-app>

    @section('content')
    {{-- Profile --}}
    <div class="relative mb-4">
        <div class="relative">
            <img class="rounded-lg mb-4" src="/images/twitterlogo.png" alt="">
            <img src="{{$user->avatar}}"
                class="rounded-full mr-2 absolute bottom-0 transform -translate-x-1/2 translate-y-1/2" style="left: 50%;
                width: 150px;
                height: 150px;">
            <form action="{{$user->path('update-banner')}}" method="post" class="absolute top-0"
                enctype="multipart/form-data">
                @csrf
                @method("PATCH")
                <input type="file" name="banner" id="banner">
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>


        <div class=" flex justify-between items-center mb-6">
            <div>
                <h2 class="font-bold text-2xl">{{ $user->name}}</h2>
                <p class="text-sm">Motto</p>
            </div>
            <div class="flex ">
                @can ('edit', $user)
                <form action="{{ $user->path('edit') }}">
                    <button class=" rounded-full border border-gray-200 shadow text-black py-2 px-4">EDIT
                        PROFILE</button>
                </form>
                @endcan

                <x-follow_button :user="$user">
                </x-follow_button>
            </div>
        </div>

        <p class="text-sm">Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis odio fuga at dignissimos
            tempora rem, ipsum
            inventore amet eius, debitis perferendis sed laborum fugiat voluptates architecto molestiae. Praesentium,
            iste
            quibusdam.</p>
    </div>

    {{-- Timeline --}}
    @include('timeline',[
    //kirimkan tweets dari user , diambil dari relasi
    'tweets' => $user->tweets
    ])
</x-app>