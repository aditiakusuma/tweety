<x-app>

    @section('content')
    {{-- Profile --}}
    <div class=" mb-4">
        <div class="relative">

            <div class="">
                <img class="rounded-lg mb-4 relative" src="{{$user->banner}}" alt="banner" id="banner">
                <x-edit-banner-button :user="current_user()" />
            </div>
            <img src="{{$user->avatar}}"
                class="rounded-full mr-2 absolute bottom-0 transform -translate-x-1/2 translate-y-1/2" style="left: 50%;
                width: 150px;
                height: 150px;">

        </div>


        <div class=" flex justify-between items-center mb-6">
            <div>
                <h2 class="font-bold text-2xl">{{ $user->name}}</h2>
                <p class="text-sm">Motto</p>
            </div>
            <div class="flex ">
                @can ('edit', $user)
                <form action="{{ $user->path('edit') }}">
                    <button class=" rounded-full border border-gray-200 shadow text-black py-2 px-4">EDIT
                        PROFILE</button>
                </form>
                @endcan

                <x-follow_button :user="$user">
                </x-follow_button>
            </div>
        </div>
        <div class="">
            <form action="{{$user->path('update-description')}}" method="post">
                @csrf
                @method('PATCH')
                <textarea class=" shadow-outline shadow-sm" name="description" id="desc"
                    style="width: -webkit-fill-available;">{{$user->description}}</textarea>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>

        </div>

    </div>

    {{-- Timeline --}}
    @include('timeline',[
    //kirimkan tweets dari user , diambil dari relasi
    'tweets' => $user->tweets
    ])
</x-app>