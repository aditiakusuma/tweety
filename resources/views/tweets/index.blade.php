<x-app>
    <div>
        {{-- tweet Pannel --}}
        @include('new-post-panel')
        <div class="border-gray-300 rounded-lg">
            {{-- Tweets --}}
            {{-- @foreach ($tweets as $tweet)
            @include('tweet')
            @endforeach --}}
            @include('timeline')

        </div>
    </div>

</x-app>