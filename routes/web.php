<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->group(function () {
    Route::get('/tweets', 'TweetController@index')->name('home');
    Route::post('/tweets', 'TweetController@store');
    Route::post('/profiles/{user:username}/follow', 'FollowsController@store');
    Route::get('/profiles/{user:username}/edit', 'ProfilesController@edit');
    Route::patch('/profiles/{user:username}', 'ProfilesController@update');
    Route::post('/tweets/{tweet}/like', 'TweetLikesController@store');
    Route::delete('/tweets/{tweet}/like', 'TweetLikesController@destroy');

    Route::get('/profiles/{user:username}/edit-banner', 'ProfilesController@editBanner');
    Route::patch('/profiles/{user:username}/update-banner', 'ProfilesController@updateBanner');
    Route::get('/profiles/{user:username}/edit-description', 'ProfilesController@editDescription');
    Route::patch('/profiles/{user:username}/update-description', 'ProfilesController@updateDescription');
});

Route::delete('/tweets/{tweet}/delete', 'TweetController@destroy');

Route::get('/profiles/{user:username}', 'ProfilesController@show')->name('profile');

Route::get('/explore', 'ExploreController@index')->name('explore');

Auth::routes();
